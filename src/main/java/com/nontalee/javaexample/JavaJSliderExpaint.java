/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.javaexample;

/**
 *
 * @author nonta
 */
import javax.swing.*;

public class JavaJSliderExpaint extends JFrame {

    public JavaJSliderExpaint() {
        JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 50, 25);
        slider.setMinorTickSpacing(2);
        slider.setMajorTickSpacing(10);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);

        JPanel panel = new JPanel();
        panel.add(slider);
        add(panel);
    }

    public static void main(String s[]) {
        JavaJSliderExpaint frame = new JavaJSliderExpaint();
        frame.pack();
        frame.setVisible(true);
    }
}
