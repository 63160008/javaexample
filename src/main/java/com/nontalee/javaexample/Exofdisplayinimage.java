/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.javaexample;

/**
 *
 * @author nonta
 */
import java.awt.*;
import javax.swing.JFrame;

public class Exofdisplayinimage extends Canvas {

    public void paint(Graphics g) {

        Toolkit t = Toolkit.getDefaultToolkit();
        Image i = t.getImage("p3.gif");
        g.drawImage(i, 120, 100, this);

    }

    public static void main(String[] args) {
        Exofdisplayinimage m = new Exofdisplayinimage();
        JFrame f = new JFrame();
        f.add(m);
        f.setSize(400, 400);
        f.setVisible(true);
    }

}
